const pluginTester = require("babel-plugin-tester"),
    myPlugin = require("../src/index"),
    path = require("path");

pluginTester({
    plugin: myPlugin,
    fixtures: path.join(__dirname, "__fixtures__")
});
