const varName = "React$createElement";
function isReactDefaultCreateElement(t, { node, parent }) {
  if (!t.isIdentifier(node.property, { name: "createElement" })) return false;
  if (!t.isIdentifier(node.object, { name: "React" })) return false;
  // console.log(parent.type);
  if (t.isVariableDeclarator(parent)) return false;
  return true;
}
module.exports = function(plugin) {
  const { types: t, template } = plugin;
  return {
    name: "optimize-react-create-element",
    pre() {
      this.counter = 0;
      this.lastRequire = null;
    },
    visitor: {
      CallExpression(path) {
        if (
          t.isIdentifier(path.node.callee, { name: "require" }) &&
          t.isStringLiteral(path.node.arguments[0], {
            value: "react"
          })
        ) {
          if (this.lastRequire) return;
          let lastRequire;
          if (!t.isVariableDeclarator(path.parent)) {
            lastRequire = path.findParent(p => t.isProgram(p.parent));
            lastRequire.insertAfter(
              template(`var ${varName} = _react.default.createElement;`)()
            );
          } else {
            const originalVarName = path.parent.id.name;
            lastRequire = path.findParent(p => t.isProgram(p.parent));
            lastRequire.insertAfter(
              template(`var ${varName} = ${originalVarName}.createElement;`)()
            );
          }
          this.lastRequire = lastRequire;
        }
      },
      ImportDeclaration(path) {
        if (this.lastRequire) return;
        if (t.isStringLiteral(path.node.source, { value: "react" })) {
          path.insertAfter(template(`var ${varName} = React.createElement;`)());
          this.lastRequire = path;
        }
      },
      MemberExpression: {
        enter(path) {
          // if (isReactDefaultCreateElement(t, path)) this.counter++;
          if (this.lastRequire && isReactDefaultCreateElement(t, path)) {
            this.counter++;
            path.replaceWith(t.identifier(varName));
          }
        }
      }
    },
    post() {
      if (this.counter > 0 && this.lastRequire === null) {
        throw new Error(
          `Variable doesn't defined. Check file ${this.filename}`
        );
      }
    }
  };
};
